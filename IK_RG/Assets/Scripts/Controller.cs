﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    [Range(0,20f)]
    public float speed = 6f;
    public float turnSmoothTime = 0.1f;
    public Transform body;
    protected BodyScript bodyScript;

    private void Start()
    {
        bodyScript = (BodyScript)body.GetComponent("BodyScript");

    }

    // Update is called once per frame
    void Update()
    {
        bool move = Input.GetKey(KeyCode.W);
        bool left = Input.GetKey(KeyCode.A);
        bool right = Input.GetKey(KeyCode.D);



        if (move)
        {
            transform.position += transform.forward * speed * Time.deltaTime;

        }
        if (left)
        {
            transform.RotateAround(transform.position, transform.up, -speed * 10 * Time.deltaTime);
            bodyScript.RotateVectors(transform.position, transform.up, -speed * 10 * Time.deltaTime);
        }
        else if (right)
        {

            transform.RotateAround(transform.position, transform.up, speed * 10*Time.deltaTime);
            bodyScript.RotateVectors(transform.position, transform.up, speed * 10 * Time.deltaTime);
        }


    }
}
