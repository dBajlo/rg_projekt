﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetScript : MonoBehaviour
{
    protected float maxRaycast = 100;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position+Vector3.up*3, Vector3.down, out hit, maxRaycast))
        {
            transform.position = hit.point + new Vector3(0,0.5f,0);
    
        }
        else if (Physics.Raycast(transform.position + Vector3.down, Vector3.up, out hit, maxRaycast))
        {
            //Debug.DrawRay(transform.position, Vector3.down * hit.distance, Color.yellow);

            transform.position = hit.point + new Vector3(0, 0.5f, 0);

        }
        
    }
}
