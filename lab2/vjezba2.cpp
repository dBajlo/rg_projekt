#include <stdio.h>
#include <stdlib.h>
#include <GL/freeglut.h>
#include <GL/glut.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/mat4x3.hpp>
#include <glm/mat3x4.hpp>
#include <glm/mat3x3.hpp>
#include <glm/gtx/string_cast.hpp>
#include <math.h>
#include <limits>
#include <stack>

using namespace std;


GLuint window;
GLuint width = 300, height = 300;

float minx, miny, minz, maxx, maxy, maxz;
float srednjix, srednjiy, srednjiz;
float maxRaspon;
float maxRasponBspline;
glm::mat4x4 matT;
glm::mat4x4 matP;
float screenCenterY;
float screenCenterX;
bool removePoligons = false;
bool useDCM = true;
bool runAnimation = false;
bool moveAttractors = false;

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void myLine(GLint x1, GLint y1, GLint x2, GLint y2);
void loadData();
void drawObject();
void drawParticles();

void printVector(vector<string>);
void calculateViewTransform();
void calculateViewTransform2();
void postaviPozicije();
int wasteTime();





typedef struct{
	float x;
	float y; 
	float z;
} Vrh;

void printVrh(Vrh);
Vrh translateToCenter(Vrh vrh);
Vrh projectVertex(Vrh vrh);

typedef struct{
	int vrh1;
	int vrh2;
	int vrh3;
} Poligon;




//===========================================================

float randomFloat(){
	return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}

float squareDistance(glm::vec3 a, glm::vec3 b){
	return (a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y) + (a.z-b.z)*(a.z-b.z);
}

class Attractor{
	public:
		float mass;
		glm::vec3 position;
		float size;
		bool direction;
		int lb;
		int rb;
		float step;
		Attractor(float mass, glm::vec3 position, int lb, int rb, float step, bool direction){
			if(mass<0){
				size = -5.0f*mass;
			}
			else{
				size = 5.0f*mass;
			}
			
			this->mass = mass;
			this->position = position;
			this->direction = direction;
			this->lb = lb;
			this->rb = rb;
			this->step = step;
		}

		void move(){
			//smjer true je lijevi, false je desni

			if(direction){
				position.x -= step;
			}
			else{
				position.x += step;
			}
			if(position.x < lb || position.x > rb){
				direction = !direction;
			}
		}
};

vector<Attractor*> attractors;
const float gravConst = 0.07f;

class Particle {        
  public:          
    glm::vec3 velocity;  
    Vrh position;  
    int ttl;   
	int lifespan;
	int id;
	float mass;
    Particle(glm::vec3 velocity, Vrh position, int ttl, int id) { 
      this->velocity = velocity;
	  this->position = position;
	  this->ttl = ttl;
	  this->lifespan = ttl;
	  this->id = id;
	  this->mass = 1.0f;
    }
	void print(){
		cout<<"========================="<<endl;
		cout<<"Cestica broj: "<<this->id<<endl;
		cout<<"Vektor brzine: "<<glm::to_string(velocity)<<endl;
		cout<<"Pozicija: "<<endl;
		printVrh(position);
		cout<<"TTL: "<<this->ttl<<endl;
	}
	//izvede jednu iteraciju i vrati true ako ttl nije 0
	bool step_isAlive(){
		this->ttl--;
		if(this->ttl == 0){
			return false;
		}
		glm::vec3 posVec = glm::vec3(position.x,position.y,position.z);
		glm::vec3 totalForce = glm::vec3(0,0,0);
		for(int i = 0; i < attractors.size(); i++){
			Attractor* a = attractors[i];
			float dst = squareDistance(posVec, a->position);
			if(dst<0.00001f){
				dst = 0.00001f;
			}
			totalForce += gravConst*glm::normalize(a->position - posVec)*mass*a->mass/dst;

		}
		//cout<<"Total force vector "<<glm::to_string(totalForce)<<endl;
		glm::vec3 acceleration = totalForce/mass;

		velocity += acceleration;

		this->position.x += this->velocity.x;
		this->position.y += this->velocity.y;
		this->position.z += this->velocity.z;
		return true;
	}
};


class ParticleFactory{
	public:
		//donja i gornja granica poligona u kojem se generiraju cestice
		Vrh polyLB;
		Vrh polyUB;
		int minTTL;
		int maxTTL;
		int particleCounter;
		ParticleFactory(Vrh polyLB, Vrh polyUB, int minTTL, int maxTTL){
			this->polyLB = polyLB;
			this->polyUB = polyUB;
			this->minTTL = minTTL;
			this->maxTTL = maxTTL;
			this->particleCounter = 0;
		}
		Particle* create(){
			//rand broj od 0.0 do 1.0
			float r1 = randomFloat();
			float r2 = randomFloat();
			float r3 = randomFloat();
			float x = polyLB.x + (polyUB.x - polyLB.x)*r1;
			float y = polyLB.y + (polyUB.y - polyLB.y)*r2;
			float z = polyLB.z + (polyUB.z - polyLB.z)*r3;
			int ttl = minTTL + rand() % (maxTTL-minTTL);
			float vx = randomFloat()/5;
			float vz = randomFloat()/5;
			float vy = randomFloat()/5;
			glm::vec3 velocity = glm::vec3(vx,vy,vz);
			Vrh position = {x,y,z};
			particleCounter++;
			return new Particle(velocity, position, ttl, particleCounter);

		}
};



//=============================================================
const int brCestica = 50;
ParticleFactory* factory;
Particle* particleArray[brCestica] = {};
stack<int> indexStack;


vector<Vrh> vrhovi;
vector<Vrh> pomaknutiVrhovi;
vector<Poligon> poligoni;
vector<Vrh> prikazaniVrhovi;
vector<glm::vec3> kontrolniPoligon;

glm::vec3 pocetnaOrijentacija;
glm::vec3 orijentacija;
vector<glm::vec3> osiRotacije;
vector<double> kuteviRotacije;
int iteracijaAnimacije = 0;
Vrh glediste;
Vrh ociste;
vector<Vrh> pozicijeOcista;
int indexOcista = 0;
glm::vec3 viewUp;


vector<string> split(string str, string delim)
{
	
    vector<string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == string::npos) pos = str.length();
        string token = str.substr(prev, pos-prev);
        
        if (!token.empty()) tokens.push_back(token);
        prev = pos + delim.length();
        
    }
    while (pos < str.length() && prev < str.length());

	

    return tokens;
}

//===================TU JEE===================

//vrati true ako se updateala bar jedna cestica
bool updateParticles(Particle* particles[], int n){
	bool ret = false;
	//cout<<"usli u update"<<endl;
	for(int i = 0; i < n; i++){
		Particle* p = particles[i];
		if(p==NULL) continue;
		ret = true;
		if(!p->step_isAlive()){
			delete p;
			indexStack.push(i);
			particleArray[i] = NULL;
			//cout<<"Obrisana cestica i oslobodjen indeks "<<i<<endl;
			
		}
	}
	if(moveAttractors){
		for(int i = 0; i < attractors.size(); i++){
			attractors[i]->move();

		}
	}
	
	myReshape(width,height);
	return ret;
}

void addParticle(Particle* p){
	if(indexStack.size()==0) return;
	int i = indexStack.top();
	indexStack.pop();
	particleArray[i] = p;
	//cout<<"Uzet indeks "<<i<<" i dodana cestica u array"<<endl;
}


void printParticleArray(Particle* p[], int n){
	for(int i = 0; i < n; i++){
		cout<<"=======ARRAY======"<<endl;
		if(p[i]!=NULL){
			cout<<"INDEKS "<<i<<endl;
			p[i]->print();
		}
		
	}
}

float birthRate = 0.6f;
void nextIter(){
	int n = brCestica;
	updateParticles(particleArray,n);
	float r = randomFloat();
	while(indexStack.size()!=0 && r <= birthRate){
		addParticle(factory->create());
		r = randomFloat();
	}
}



void runF(){
	while(runAnimation){
		nextIter();
		wasteTime();
	}
}

void testParticle(){
	Vrh LB = {-5,4,-5};
	Vrh UB = {5,5,5};
	int n = brCestica;


	factory = new ParticleFactory(LB,UB, 60, 80);



	for(int i = 0; i < n; i++){
		indexStack.push(i);
	}



	for(int i = 0; i < n; i++){
		addParticle(factory->create());
	}
	
	glm::vec3 apos = glm::vec3(0,0,0);
	Attractor* a = new Attractor(5, apos, -3, 4, 0.2, true);
	attractors.push_back(a);
	glm::vec3 apos2 = glm::vec3(-3,-3,0);
	a = new Attractor(7, apos2, -5,5,0.1,false);
	attractors.push_back(a);

	apos2 = glm::vec3(-3,-5,0);
	a = new Attractor(10, apos2, -5,5,0.1,false);
	attractors.push_back(a);
	
}

int main(int argc, char ** argv)
{
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);

	window = glutCreateWindow("Vjezba 2");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutMouseFunc(myMouse);
	glutKeyboardFunc(myKeyboard);
	loadData();
	postaviPozicije();
	//printVrh(glediste);
	testParticle();
	glutMainLoop();
	return 0;
}



void calculateViewTransform(){
	glm::mat4x4 T1 = glm::mat4x4(1, 0, 0, 0,
								 0, 1, 0, 0,
								 0, 0, 1, 0,
								 -ociste.x, -ociste.y, -ociste.z, 1);
	//glm je column major
	T1 = glm::transpose(T1);
	glm::vec3 O = glm::vec3(ociste.x, ociste.y, ociste.z);
	glm::vec3 G = glm::vec3(glediste.x, glediste.y, glediste.z);
	glm::vec3 Z = G-O;
	glm::vec3 zk = glm::normalize(Z);
	glm::vec3 vk = glm::normalize(viewUp);
	glm::vec3 xk = glm::cross(zk, vk);
	glm::vec3 yk = glm::cross(xk, zk);

	glm::mat4x4 Ruku = glm::mat4x4(xk.x, yk.x, zk.x, 0,
								   xk.y, yk.y, zk.y, 0,
								   xk.z, yk.z, zk.z, 0,
								   0, 0, 0, 1);
	Ruku = glm::transpose(Ruku);
	glm::mat4x4 Tz = glm::mat4x4(1, 0, 0, 0,
								 0, 1, 0, 0,
								 0, 0, -1, 0,
								 0, 0, 0, 1);
	Tz = glm::transpose(Tz);

	matT = T1*Ruku*Tz;

	float H = glm::length(Z);
	glm::mat4x4 P = glm::mat4x4(1, 0, 0, 0,
								0, 1, 0, 0,
								0, 0, 0, (1/H),
								0, 0, 0, 0);
	P = glm::transpose(P);

	matP = P;

	if(false){
		cout<<glm::to_string(viewUp)<<endl;
		cout<<"Ukupna matrica: "<<glm::to_string(matT)<<endl;
			glm::vec4 tocka468 = glm::vec4(4, 6, 8, 1);
			cout<<"Tocka "<<glm::to_string((tocka468*matT))<<endl;
			glm::vec4 AP = (tocka468*matT)*P;
			AP.x = AP.x/AP.w;
			AP.y = AP.y/AP.w;
			cout<<"Tocka projekcija: "<<AP.x<<", "<<AP.y<<endl;
	}
}

void postaviPozicije(){
	Vrh vrh = Vrh{20,0,-20};
	pozicijeOcista.push_back(vrh);
	vrh = Vrh{20,20,-20};
	pozicijeOcista.push_back(vrh);
	vrh = Vrh{20,20,-40};
	pozicijeOcista.push_back(vrh);
	vrh = Vrh{0,0,-20};
	pozicijeOcista.push_back(vrh);
}

void loadModel(string fileName){

	bool firstVertex = true;
	string line;
	Vrh suma = {0,0,0};
	ifstream myfile (fileName);
	if (myfile.is_open()){
		while (getline (myfile,line)){
			if(line[0] == 'v'){
				vector<string> vertices = split(line, " ");
				
				Vrh vrh = {stof(vertices[1])*100,stof(vertices[2])*100,stof(vertices[3])*100};
				vrhovi.push_back(vrh);
				prikazaniVrhovi.push_back(vrh);
				
				if(firstVertex){
					firstVertex = false;
					minx = vrh.x;
					maxx = vrh.x;
					miny = vrh.y;
					maxy = vrh.y;
					minz = vrh.z;
					maxz = vrh.z;
				}
				else{
					if(minx > vrh.x){
						minx = vrh.x;
					}
					if(maxx < vrh.x){
						maxx = vrh.x;
					}
					if(miny > vrh.y){
						miny = vrh.y;
					}
					if(maxy < vrh.y){
						maxy = vrh.y;
					}
					if(minz > vrh.z){
						minz = vrh.z;
					}
					if(maxz < vrh.z){
						maxz = vrh.z;
					}
				}
			}
			else if(line[0] == 'f'){
				vector<string> polygons = split(line, " ");
				
				Poligon poligon = {stoi(polygons[1]),stoi(polygons[2]),stoi(polygons[3])};
				poligoni.push_back(poligon);
			}
		}
		myfile.close();
		
		
	}
	//ispis ucitanih podataka
	if(false){
		for(int i = 0; i < vrhovi.size(); i++){
		cout << "Vrh "<<i<<":  "<<vrhovi[i].x << " "<< vrhovi[i].y << " " << vrhovi[i].z <<endl;
		}

		for(int i = 0; i < poligoni.size(); i++){
			cout << "Poligon "<<i<<":  " <<poligoni[i].vrh1<<" "<<poligoni[i].vrh2<<" "<< poligoni[i].vrh3 << endl;
		}
	}
	

	srednjix = (minx + maxx)/2;
	srednjiy = (miny + maxy)/2;
	srednjiz = (minz + maxz)/2;
	maxRaspon = maxx - minx;
	float yRaspon = maxy - miny;
	if(maxRaspon < yRaspon){
		maxRaspon = yRaspon;
	}
	float zRaspon = maxz - minz;
	if(maxRaspon < zRaspon){
		maxRaspon = zRaspon;
	}

	for(int i = 0; i < vrhovi.size(); i++){
		vrhovi[i].x = (vrhovi[i].x-srednjix)*(2/maxRaspon);
		vrhovi[i].y = (vrhovi[i].y-srednjiy)*(2/maxRaspon);
		vrhovi[i].z = (vrhovi[i].z-srednjiz)*(2/maxRaspon);
		//cout << "Normirani vrh "<<i<<":  "<<vrhovi[i].x << " "<< vrhovi[i].y << " " << vrhovi[i].z <<endl;

	}

	
}

void printVector(vector<string> vec){
	for(int i=0; i<vec.size(); i++){
		cout<<vec[i]<<", ";
	}
	cout<<"\n"<<endl;
}



void loadData(){
	
	string line;
	ifstream myfile ("objekti/data1.txt");
	if (myfile.is_open()){
		vector<string> imeModela;
		vector<string> imeBsplajn;
		while (getline (myfile,line)){

			if(line[0] == 'G'){
				vector<string> koordinate = split(line, " ");
		
				glediste.x = stof(koordinate[1]);
				glediste.y = stof(koordinate[2]);
				glediste.z = stof(koordinate[3]);
			}
			else if(line[0] == 'O'){
				vector<string> koordinate = split(line, " ");
				ociste.x = stof(koordinate[1]);
				ociste.y = stof(koordinate[2]);
				ociste.z = stof(koordinate[3]);
			}
			else if(line[0] == 'M'){
				imeModela = split(line, " ");
			}
			else if(line[0] == 'U'){
				vector<string> koordinate = split(line, " ");
				
				viewUp = glm::vec3(stof(koordinate[1]), stof(koordinate[2]), stof(koordinate[3]));
				
			}
			else if(line[0] == 'B'){
				imeBsplajn = split(line, " ");
			}
			else if(line[0] == 'o'){
				vector<string> koordinate = split(line, " ");
				pocetnaOrijentacija = {stof(koordinate[1]),stof(koordinate[2]),stof(koordinate[3])};
			}
		}
		myfile.close();
		loadModel("objekti/"+imeModela[1]);
	}
		
}


glm::vec3 scaleVector(glm::vec3 vec, float maxRaspon){
	float scale = screenCenterX;
	if(screenCenterY < screenCenterX){
		scale = screenCenterY;
	}
	scale = scale/maxRaspon*2;
	glm::vec3 vrh = {vec.x, vec.y, vec.z};
	vrh.x = vrh.x * scale;
	vrh.y = vrh.y * scale;
	vrh.z = vrh.z * scale;
	return vrh;
}





//=====================================================

Vrh projectVertex(Vrh vrh){
	glm::vec4 v = glm::vec4(vrh.x, vrh.y, vrh.z, 1);
	v = v*matT*matP;
	Vrh pov = {v.x/v.w, v.y/v.w, v.z};
	return pov;
}

Vrh translateToCenter(Vrh vrh){
	float scale = screenCenterX;
	if(screenCenterY < screenCenterX){
		scale = screenCenterY;
	}

	scale /= 4;
	
	vrh.x = vrh.x * scale + screenCenterX;
	vrh.y = vrh.y * scale + screenCenterY;
	return vrh;
}

void printVrh(Vrh vrh){
	cout<<vrh.x<<", "<<vrh.y<<", "<<vrh.z<<endl;
}


float testPoint(glm::vec4 point, Vrh v1,Vrh v2,Vrh v3){
	

	glm::vec3 vektor1 = glm::vec3( v2.x - v1.x, v2.y - v1.y, v2.z - v1.z );
	glm::vec3 vektor2 = glm::vec3( v3.x - v1.x, v3.y - v1.y, v3.z - v1.z );

	glm::vec3 normala = glm::cross(vektor1, vektor2);

	//cout << "A, B, C: "<<glm::to_string(normala)<<endl;

	float A = normala.x;
	float B = normala.y;
	float C = normala.z;

	//D = -Ax - By - Cz
	float D = -A* v1.x -B* v1.y -C* v1.z;

	glm::vec4 R = glm::vec4(A, B, C, D);

	float pointPosition = glm::dot(point, R);
	return pointPosition;
		
}

void drawObject(){
	//cout<<"pozvan draw"<<endl;
	calculateViewTransform();
	screenCenterY = height / 2;
	screenCenterX = width / 2;
	glPointSize(1.0);					
	glColor3f(0.0f, 0.0f, 0.0f);	
	
	if(pomaknutiVrhovi.size()==0){
		pomaknutiVrhovi = vrhovi;
	}

	for(int i = 0; i < poligoni.size(); i++){
		Vrh v1 = translateToCenter(projectVertex(pomaknutiVrhovi[poligoni[i].vrh1 - 1]));
		Vrh v2 = translateToCenter(projectVertex(pomaknutiVrhovi[poligoni[i].vrh2 - 1]));
		Vrh v3 = translateToCenter(projectVertex(pomaknutiVrhovi[poligoni[i].vrh3 - 1])); 

		if(false and i==0){
			cout<<"Prije ";
			printVrh(projectVertex(vrhovi[poligoni[i].vrh1 - 1]));
			cout<<"Samo centriran ";
			printVrh(translateToCenter(vrhovi[poligoni[i].vrh1 - 1]));
			cout<<"Sve ";
			printVrh(v1);
		}

		if(removePoligons && testPoint(glm::vec4(ociste.x, ociste.y, ociste.z, 1), v1,v2,v3)>=0){
			continue;
		}
	

		glBegin(GL_LINE_LOOP);
			glVertex2f(v1.x , v1.y);
			glVertex2f(v2.x , v2.y);
			glVertex2f(v3.x , v3.y);
		glEnd();
	}

}


void drawParticles(){

	calculateViewTransform();
	screenCenterY = height / 2;
	screenCenterX = width / 2;
	glPointSize(5.0);					
		
	
	//cout<<"usli u draw particle"<<endl;

	for(int i = 0; i < attractors.size(); i++){
		Attractor* a = attractors[i];
		Vrh pozicija = {a->position.x,a->position.y,a->position.z};
		Vrh v1 = translateToCenter(projectVertex(pozicija));

		glColor3f(0.0f, 1.0f, 0.0f);
		glPointSize(a->size);
		glBegin(GL_POINTS);
			glVertex2f(v1.x , v1.y);

		glEnd();
	}

	for(int i = 0; i < brCestica; i++){
		Particle* p = particleArray[i];
		if(p==NULL) continue;
		Vrh v1 = translateToCenter(projectVertex(p->position));

		if(false){
			cout<<"================="<<endl;
			cout<<"Prije ";
			printVrh(p->position);
			cout<<"Poslije ";
			printVrh(v1);
		}
		float intensity = 1.0f - p->ttl / (float)p->lifespan;
		glPointSize(1.0+ 10.0*intensity);
		
							
		glColor3f(intensity, intensity, 1.0f);
		glBegin(GL_POINTS);
			glVertex2f(v1.x , v1.y);

		glEnd();
	}

}

void myDisplay()
{

	glFlush();
}


void myReshape(int w, int h)
{

	width = w; height = h;               							
	glViewport(0, 0, width, height);	
	
	glMatrixMode(GL_PROJECTION);		
	glLoadIdentity();					
	glOrtho(0, width, 0, height, 0, 1); 	
	glMatrixMode(GL_MODELVIEW);			
	glLoadIdentity();					

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);		
	glPointSize(1.0);					
	glColor3f(0.0f, 0.0f, 0.0f);	

	drawParticles();
	glFlush();

}

	

void rotateGlediste(char os, int smjer){
	float sin15 = smjer*0.08716f;
	float cos15 = 0.99619f; 
	glm::mat4x4 matRot;
	if(os=='y'){
		matRot= glm::mat4x4(cos15, 0, sin15, 0,
							0, 1, 0, 0,
							-sin15, 0, cos15, 0,
							0, 0, 0, 1);
	}
	else if(os=='x'){
		matRot= glm::mat4x4(1, 0, 0, 0,
							0, cos15, -sin15, 0,
							0, sin15, cos15, 0,
							0, 0, 0, 1);
	}
	matRot = glm::transpose(matRot);
	glm::mat4x4 T1 = glm::mat4x4(1, 0, 0, 0,
								 0, 1, 0, 0,
								 0, 0, 1, 0,
								 -ociste.x, -ociste.y, -ociste.z, 1);
	T1 = glm::transpose(T1);

	glm::mat4x4 T2 = glm::mat4x4(1, 0, 0, 0,
								 0, 1, 0, 0,
								 0, 0, 1, 0,
								 ociste.x, ociste.y, ociste.z, 1);
	T2 = glm::transpose(T2);
	glm::mat4x4 matUku = T1*matRot*T2;
	glm::vec4 G = glm::vec4(glediste.x, glediste.y, glediste.z, 1);
	G = G*matUku;
	glediste.x = G.x;
	glediste.y = G.y;
	glediste.z = 0;

	
}

int wasteTime(){
	int antiOptimizationSum = 0;
	int n = 20000000;

	for(int i=0; i < n; i++){
		antiOptimizationSum+=1;
	}
	return antiOptimizationSum;
}

Vrh vectorToVrh(glm::vec3 v){
	Vrh vrh = {v.x, v.y, v.z};
	return vrh;
}

glm::vec3 vrhToVector(Vrh v){
	glm::vec3 vec = {v.x, v.y, v.z};
	return vec;
}

void startAnimation(){

	int n = 5;
	for(int i = 0; i < n; i++){
		
		//NIATA NE RAED
		
		myReshape(width, height);
		glFlush();
		wasteTime();
	}
}

void myMouse(int button, int state, int x, int y)
{
	
	
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	int speed = 10;
	float lookSpeed = 0.5f;
	//printVrh(glediste);
	switch (theKey)
	{
	case 'w':
		
		ociste.z += speed;
		break;

	case 'd':
		ociste.x += speed;
		break;

	case 's':
		ociste.z -= speed;
		break;

	case 'a':
		ociste.x -= speed;
		break;
	case 'e':
		ociste.y -= speed;
		break;

	case 'q':
		ociste.y += speed;
		break;
	case 'r':
		ociste = pozicijeOcista[indexOcista];
		indexOcista++;
		if(indexOcista>=pozicijeOcista.size()){
			indexOcista=0;
		}
		glediste.x = 0;
		glediste.y = 0;
		glediste.z = 0;
		break;
	case 'i':
		glediste.y += lookSpeed;
		break;
	
	case 'k':
		glediste.y -= lookSpeed;
		break;
	
	case 'j':
		glediste.x += lookSpeed;
		break;
	
	case 'l':
		glediste.x -= lookSpeed;
		break;
	
	case 'b':
		startAnimation();
		break;
	case 'f':
		removePoligons = !removePoligons;
		break;
	case 'n':
		useDCM = !useDCM;
		break;
	case 'p':
		moveAttractors = false;
		nextIter();
		break;
	case 'o':
		moveAttractors = true;
		nextIter();
		break;
	}
	myReshape(width, height);

	glFlush();
}







