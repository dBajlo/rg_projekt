﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Structs 
{
	//used to store the position of a vertex and it's index in the vertices list, NOT USED ANYMORE
	public struct VertexIndex
	{
		Vector3 vertex;
		int index;

		public VertexIndex(Vector3 vertex, int index)
		{
			this.vertex = vertex;
			this.index = index;
		}

		public Vector3 getVertex()
		{
			return vertex;
		}

		public int getIndex()
		{
			return index;
		}
	}

	public struct Triangle
	{
		public Vector3 v0;
		public Vector3 v1;
		public Vector3 v2;
	};

	public struct TerrainChange
	{
		Vector3 position;
		float radius;
		//true if adding material, false if subtracting
		bool addedMaterial;

		public TerrainChange(Vector3 position, float radius, bool addedMaterial)
		{
			this.position = position;
			this.radius = radius;
			this.addedMaterial = addedMaterial;
		}

		public Vector3 getPosition()
		{
			return position;
		}

		public float getRadius()
		{
			return radius;
		}

		public bool getMaterial()
		{
			return addedMaterial;
		}
	}
}
