﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyScript : MonoBehaviour
{
    public Transform target1;
    public Transform target2;
    public Transform target3;
    public Transform target4;
    protected Transform[] targets;
    protected int cnt = 2;
    protected Vector3 downRight;
    protected Vector3 downForward;
    protected Vector3 oldCentroid;


    
 

    
    public void RotateVectors(Vector3 point, Vector3 axis, float angle)
    {
        downRight = Quaternion.AngleAxis(angle, axis) * downRight;
        downForward = Quaternion.AngleAxis(angle, axis) * downForward;
    }
    private Vector3 ProjectVector(Vector3 vec, Vector3 normal)
    {
        var plane = new Plane(normal, transform.position);
        Vector3 ret = plane.ClosestPointOnPlane(vec);
        return ret;
    }
    private void Start()
    {
        targets = new Transform[4];
        targets[0] = target1;
        targets[1] = target2;
        targets[2] = target3;
        targets[3] = target4;
        oldCentroid = Vector3.zero;
        for (int i = 0; i < 4; i++)
        {
            oldCentroid += targets[i].position;
        }
        oldCentroid /= 4;
        downRight = ProjectVector(oldCentroid, -transform.right) - transform.position;
        downForward = ProjectVector(oldCentroid, -transform.forward) - transform.position;
    }


    // Update is called once per frame
    void Update()
    {
        Vector3 centroid = Vector3.zero;
        
        for (int i = 0; i < 4; i++)
        {
            centroid += targets[i].position;
        }
        centroid /= 4;

        //moving origin
        transform.position += new Vector3(0, centroid.y - oldCentroid.y, 0);
        oldCentroid = centroid;


        //pitch

        Vector3 direction =  ProjectVector(centroid, -transform.right) - transform.position;
        var angle = Vector3.SignedAngle(downRight, direction, transform.right);
        
        if (angle!=0)
        {
            transform.RotateAround(transform.position, transform.right, angle);
            downRight = direction;
        }

        //roll

        direction = ProjectVector(centroid, -transform.forward) - transform.position;
        angle = Vector3.SignedAngle(downForward, direction, transform.forward);

        if (angle != 0)
        {
            transform.RotateAround(transform.position, transform.forward, angle);
            downForward = direction;
        }


        //Debug.DrawRay(transform.position, down * 100, Color.yellow);
        //Debug.DrawRay(transform.position, direction * 100, Color.white);

    }
}
