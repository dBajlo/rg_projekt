﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CCD : MonoBehaviour
{
    public int ChainLength;
    public Transform Target;
    public int Iterations=10;
    public float Delta=0.01f;
    public Transform Pole;
    public bool EnableGizmos = false;
    public bool Modified = false;
    public bool UsePole = true;
    public bool Rotate = false;

    protected float[] BonesLength;
    protected float CompleteLength;
    protected Transform[] Bones;
    protected Vector3[] StartDirectionSucc;
    protected Quaternion[] StartRotationBone;
    protected Quaternion StartRotationTarget;
    protected Quaternion StartRotationRoot;
    protected float DeltaSqr;
    //strength of going back to the start position
    [Range(0, 1)]
    public float SnapBackStrength = 1f;

    private void Awake()
    {
        Init();
    }

    void Init()
    {
        Bones = new Transform[ChainLength + 1];
        BonesLength = new float[ChainLength];
        StartDirectionSucc = new Vector3[ChainLength + 1];
        StartRotationBone = new Quaternion[ChainLength + 1];
        DeltaSqr = Delta * Delta;


        if (Target == null)
        {
            Target = new GameObject(gameObject.name + "Target").transform;
            Target.position = transform.position;
        }
        StartRotationTarget = Target.rotation;
        CompleteLength = 0;

        var current = transform;
        for (var i = Bones.Length - 1; i >= 0; i--)
        {
            Bones[i] = current;
            StartRotationBone[i] = current.rotation;
            if (i == Bones.Length - 1)
            {
                //leaf
                StartDirectionSucc[i] = Target.position - current.position;
            }
            else
            {

                //mid bone
                StartDirectionSucc[i] = Bones[i + 1].position - current.position;
                BonesLength[i] = StartDirectionSucc[i].magnitude;
                CompleteLength += BonesLength[i];
            }
            current = current.parent;
        }

    }

    private void LateUpdate()
    {
        ResolveIK();
    }


    private void ResolveIK()
    {
        if (Target == null) return;
        if (BonesLength.Length != ChainLength) Init();

        var RootRot = (Bones[0].parent != null) ? Bones[0].parent.rotation : Quaternion.identity;
        var RootRotDiff = RootRot * Quaternion.Inverse(StartRotationRoot);


        //if target is out of reach strech in direction from first bone to target
        if ((Target.position - Bones[0].position).sqrMagnitude >= CompleteLength * CompleteLength)
        {
            var direction = (Target.position - Bones[0].position).normalized;
            for (int i = 1; i < Bones.Length; i++)
            {
                Bones[i].position = Bones[i - 1].position + direction * BonesLength[i - 1];
            }
        }
        else
        {
            
            if (!Modified)
            {
                for (int iteration = 0; iteration < Iterations; iteration++)
                {


                    for (int i = Bones.Length - 2; i >= 0; i--)
                    {
                        var bone = Bones[i];
                        var effector = Bones[Bones.Length - 1];
                        var targetPosition = Target.position;
                        RotateBone(effector, bone, targetPosition);

                        if ((Bones[Bones.Length - 1].position - Target.position).sqrMagnitude < DeltaSqr)
                        {
                            break;
                        }
                    }


                    if ((Bones[Bones.Length - 1].position - Target.position).sqrMagnitude < DeltaSqr)
                    {
                        break;
                    }

                }
            }
            else
            {
                //instead of always iterating through all bones
                //we iterate through the end bones more times
                int secondLast = Bones.Length - 2;
                for (int iteration = 0; iteration < Iterations; iteration++)
                {

                    string output = "";
                    for (int i = 0; i <secondLast; i++)
                    {
                        for(int j = secondLast; j > secondLast-i-2 && j >= 0; j--)
                        {
                            var bone = Bones[j];
                            output += j+" ";
                            var effector = Bones[Bones.Length - 1];
                            var targetPosition = Target.position;
                            RotateBone(effector, bone, targetPosition);

                            if ((Bones[Bones.Length - 1].position - Target.position).sqrMagnitude < DeltaSqr)
                            {
                                break;
                            }
                        }
                        
                    }
                    //Debug.Log(output);


                    if ((Bones[Bones.Length - 1].position - Target.position).sqrMagnitude < DeltaSqr)
                    {
                        break;
                    }

                }

            }
            if (UsePole && Pole != null)
            {
                for (int i = 1; i < Bones.Length - 1; i++)
                {
                    var plane = new Plane(Bones[i + 1].position - Bones[i - 1].position, Bones[i - 1].position);
                    var projectedPole = plane.ClosestPointOnPlane(Pole.position);
                    var projectedBone = plane.ClosestPointOnPlane(Bones[i].position);
                    var angle = Vector3.SignedAngle(projectedBone - Bones[i - 1].position, projectedPole - Bones[i - 1].position, plane.normal);
                    Bones[i].position = Quaternion.AngleAxis(angle, plane.normal) * (Bones[i].position - Bones[i - 1].position) + Bones[i - 1].position;
                }
            }

        }


        //set rotation
        if (Rotate)
        {
            for (int i = 0; i < Bones.Length; i++)
            {
                if (i == Bones.Length - 1)
                {
                    Bones[i].rotation = Target.rotation * Quaternion.Inverse(StartRotationTarget) * StartRotationBone[i];
                }
                else
                {
                    Bones[i].rotation = Quaternion.FromToRotation(StartDirectionSucc[i], Bones[i + 1].position - Bones[i].position) * StartRotationBone[i];
                }

            }
        }
        

    }

    public static void RotateBone(Transform effector, Transform bone, Vector3 targetPosition)
    {
        Vector3 effectorPosition = effector.position;
        Vector3 bonePosition = bone.position;
        Quaternion boneRotation = bone.rotation;

        Vector3 boneToEffector = effectorPosition - bonePosition;
        Vector3 boneToGoal = targetPosition - bonePosition;

        Quaternion fromToRotation = Quaternion.FromToRotation(boneToEffector, boneToGoal);
        Quaternion newRotation = fromToRotation * boneRotation;

        bone.rotation = newRotation;
    }

    private void OnDrawGizmos()
    {
        if (EnableGizmos)
        {
            var current = this.transform;
            for (int i = 0; i < ChainLength && current != null && current.parent != null; i++)
            {
                var scale = Vector3.Distance(current.position, current.parent.position) * 0.1f;
                Handles.matrix = Matrix4x4.TRS(current.position, Quaternion.FromToRotation(Vector3.up,
                    current.parent.position - current.position), new Vector3(scale, Vector3.Distance(current.parent.position,
                    current.position), scale));
                Handles.color = Color.green;
                Handles.DrawWireCube(Vector3.up * 0.5f, Vector3.one);
                current = current.parent;
            }
        }

    }
}
