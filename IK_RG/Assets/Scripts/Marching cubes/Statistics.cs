﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class Statistics 
{

    public static double StandardDeviation(List<double> values)
    {
        if (values.Count < 2) return 0;
        double avg = AverageValue(values);
        double sqrSum = 0;
        foreach (double d in values)
        {
            sqrSum += System.Math.Pow(d - avg, 2);
        }
        return System.Math.Sqrt(sqrSum / (values.Count - 1));
    }

    public static double AverageValue(List<double> values)
    {
        double sum = 0;
        if (values.Count < 1) return 0;
        foreach(double value in values)
        {
            sum += value;
        }
        return sum / values.Count;
    }

    public static string GetAverageAndDeviationString(List<double> values)
    {
        return System.Math.Round(Statistics.AverageValue(values),6) + " +/- " + System.Math.Round(Statistics.StandardDeviation(values),6);
    }
}
