﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
	public bool autoUpdate = true;
	[Range(1, 30)]
	public float falloffPower = 4f;
	[Range(2, 6)]
	public int perlinNum = 2;
	public bool editAlg2 = true;


	public List<Structs.TerrainChange> changes;
	public int editRadius = 5;



	
	public bool smoothTerrain = true;
	public bool flatShaded;

	float persistence = 0.5f;
	float lacunarity = 2f;
	[Range(1, 5)]
	public int octaves = 3;
	public const int maxOctaves = 5;
	public float scale = 16.43f;
	
	List<Vector3> vertices = new List<Vector3>();
	List<int> triangles = new List<int>();
	MeshFilter meshFilter;
	MeshCollider meshCollider;

	List<UnityEngine.Object> voxels = new List<UnityEngine.Object>();
	Dictionary<Vector3, int> vertexIndexDict = new Dictionary<Vector3, int>();


	public float offsetX = 64;

	public float offsetY = 64;
	
	public float offsetZ = 64;
	[Range(0, 1)]
	public float thresholdDensity = 0.5f;
	
	

	[Range(0, 10)]
	public int currentBorderWidth = 1;
	public bool isSphere;
	public bool usesFalloff = true;



	[Range(2, 100)]
	public int width = 38;
	[Range(2, 100)]
	public int height = 38;
	[Range(2, 100)]
	public int length = 38;
	float[,,] terrainMap;



	public GameObject blockPrefab;
	public GameObject blockParent;
	public bool useVoxel = false;



	private void Start()
	{


		float startTime = Time.realtimeSinceStartup;
		meshCollider = GetComponent<MeshCollider>();
		meshFilter = GetComponent<MeshFilter>();

		changes = new List<Structs.TerrainChange>();

		GenerateTerrain();


		
		
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.G))
		{
			GenerateTerrain();
		}
	}

	public void GenerateTerrain()
	{
		terrainMap = Noise.GenerateNoiseMap(width, height, length, isSphere, usesFalloff, thresholdDensity, falloffPower, octaves, scale, persistence,
				lacunarity, perlinNum, new Vector3(offsetX, offsetY, offsetZ), changes, editAlg2);
		RedrawTerrain();
		
	}

	public void RedrawTerrain()
	{

		ClearMeshData();
		DestroyVoxels();
		if (useVoxel)
		{
			GenerateVoxel();
		}
		else
		{
			CreateMeshData();

		}
		BuildMesh();
	}

	public void UpdateEdits()
	{
		float startTime = Time.realtimeSinceStartup;
		Noise.EditValues(changes[changes.Count-1], width, height, length, terrainMap);
		RedrawTerrain();

	}

	

	

	

	void GenerateVoxel()
	{
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				for (int z = 0; z < length; z++)
				{
					if (terrainMap[x,y,z]  < thresholdDensity)
					{
						GameObject block = Instantiate(blockPrefab, new Vector3(x, y, z), Quaternion.identity);
						voxels.Add(block);
						block.transform.SetParent(blockParent.transform);
					}
				}
			}
		}
	}

	void DestroyVoxels()
	{
		foreach (UnityEngine.Object voxel in voxels)
		{
			Destroy(voxel);
		}
	}

	

	void CreateMeshData()
	{
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				for (int z = 0; z < length; z++)
				{
					float[] cube = new float[8];
					for(int i = 0; i < 8; i++)
					{
						Vector3Int corner = new Vector3Int(x, y, z) + LookupTable.CornerTable[i];
						cube[i] = terrainMap[corner.x, corner.y, corner.z];
					}
					MarchCube(new Vector3Int(x, y, z), cube);
				}
			}
		}

	}

	int GetCubeConfig(float[] cube)
	{
		int configIndex = 0;
		for(int i = 0; i < 8; i++)
		{
			if(cube[i] > thresholdDensity)
			{
				configIndex |= 1 << i;
			}
		}
		return configIndex;
	}

	
	void MarchCube(Vector3Int position, float[] cube)
	{
		
		int configIndex = GetCubeConfig(cube);


		if (configIndex == 0 || configIndex == 255)
		{
			return;
		}

		

		int edgeIndex = 0;
		for(int i = 0; i < 5; i++)
		{
			for(int p = 0; p < 3; p++)
			{
				int index = LookupTable.TriangleTable[configIndex, edgeIndex];
				if(index == -1)
				{
					return;
				}

				Vector3 vert1 = position + LookupTable.EdgeTable[index, 0];
				
				Vector3 vert2 = position + LookupTable.EdgeTable[index, 1];
				Vector3 vertPosition;
				if (smoothTerrain)
				{
					float vert1Sample = cube[LookupTable.EdgeIndices[index, 0]];
					float vert2Sample = cube[LookupTable.EdgeIndices[index, 1]];
					float difference = vert2Sample - vert1Sample;
					if (difference == 0)
					{
						difference = thresholdDensity;
					}
					else
					{
						difference = (thresholdDensity - vert1Sample) / difference;
					}
					vertPosition = vert1 + ((vert2 - vert1) * difference);
				}
				else
				{
					vertPosition = (vert1 + vert2) / 2;
				}
				
				if (flatShaded)
				{
					vertices.Add(vertPosition);
					triangles.Add(vertices.Count - 1);
				}
				else
				{
					
					triangles.Add(IndexForVertex(vertPosition));
				}
				edgeIndex++;
			}
		}
		
	}

	void ClearMeshData()
	{
		vertices.Clear();
		triangles.Clear();
		vertexIndexDict.Clear();
	}

	void BuildMesh()
	{
		Mesh mesh = new Mesh();
		mesh.vertices = vertices.ToArray();
		mesh.triangles = triangles.ToArray();
		//to make it look nice and neat
		mesh.RecalculateNormals();
		meshFilter.mesh = mesh;
		meshCollider.sharedMesh = mesh;
	}

	string VertexToString(Vector3 vertex)
	{
		return vertex.x + ", " + vertex.y + ", " + vertex.z;
	}

	


	int IndexForVertex(Vector3 vertex)
	{
		
		if (vertexIndexDict.ContainsKey(vertex))
		{
			
			return vertexIndexDict[vertex];
		}
		//the vertex doesn't exist so we create it
		vertices.Add(vertex);
		int index = vertices.Count - 1;
		vertexIndexDict[vertex] = index;
		
		return index;
	}








}
