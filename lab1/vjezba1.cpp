#include <stdio.h>
#include <GL/freeglut.h>
#include <GL/glut.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/mat4x3.hpp>
#include <glm/mat3x4.hpp>
#include <glm/mat3x3.hpp>
#include <glm/gtx/string_cast.hpp>
#include <math.h>
#include <limits>

using namespace std;


GLuint window;
GLuint width = 300, height = 300;

float minx, miny, minz, maxx, maxy, maxz;
float srednjix, srednjiy, srednjiz;
float maxRaspon;
float maxRasponBspline;
glm::mat4x4 matT;
glm::mat4x4 matP;
float screenCenterY;
float screenCenterX;
bool removePoligons = false;
bool useDCM = true;

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void myLine(GLint x1, GLint y1, GLint x2, GLint y2);
void loadData();
void drawObject();
void drawLines();
void printVector(vector<string>);
void calculateViewTransform();
void calculateViewTransform2();
void postaviPozicije();
void calculateBspline();
void drawTangent();



typedef struct{
	float x;
	float y; 
	float z;
} Vrh;

void printVrh(Vrh);
Vrh translateToCenter(Vrh vrh);
Vrh projectVertex(Vrh vrh);

typedef struct{
	int vrh1;
	int vrh2;
	int vrh3;
} Poligon;

vector<Vrh> vrhovi;
vector<Vrh> pomaknutiVrhovi;
vector<Poligon> poligoni;
vector<Vrh> prikazaniVrhovi;
vector<glm::vec3> kontrolniPoligon;
vector<glm::vec3> bsplajnVrhovi;
vector<glm::vec3> bsplajnTangente;
vector<glm::vec3> bsplajnNormale;
vector<glm::vec3> bsplajnBinormale;
glm::vec3 pocetnaOrijentacija;
glm::vec3 orijentacija;
vector<glm::vec3> osiRotacije;
vector<double> kuteviRotacije;
int iteracijaAnimacije = 0;
Vrh glediste;
Vrh ociste;
vector<Vrh> pozicijeOcista;
int indexOcista = 0;
glm::vec3 viewUp;
Vrh centarBspline;

vector<string> split(string str, string delim)
{
	
    vector<string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == string::npos) pos = str.length();
        string token = str.substr(prev, pos-prev);
        
        if (!token.empty()) tokens.push_back(token);
        prev = pos + delim.length();
        
    }
    while (pos < str.length() && prev < str.length());

	

    return tokens;
}

int main(int argc, char ** argv)
{
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);

	window = glutCreateWindow("Vjezba 1");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutMouseFunc(myMouse);
	glutKeyboardFunc(myKeyboard);
	loadData();
	postaviPozicije();
	calculateBspline();
	printVrh(glediste);
	drawObject();
	glutMainLoop();
	return 0;
}



void calculateViewTransform(){
	glm::mat4x4 T1 = glm::mat4x4(1, 0, 0, 0,
								 0, 1, 0, 0,
								 0, 0, 1, 0,
								 -ociste.x, -ociste.y, -ociste.z, 1);
	//glm je column major
	T1 = glm::transpose(T1);
	glm::vec3 O = glm::vec3(ociste.x, ociste.y, ociste.z);
	glm::vec3 G = glm::vec3(glediste.x, glediste.y, glediste.z);
	glm::vec3 Z = G-O;
	glm::vec3 zk = glm::normalize(Z);
	glm::vec3 vk = glm::normalize(viewUp);
	glm::vec3 xk = glm::cross(zk, vk);
	glm::vec3 yk = glm::cross(xk, zk);

	glm::mat4x4 Ruku = glm::mat4x4(xk.x, yk.x, zk.x, 0,
								   xk.y, yk.y, zk.y, 0,
								   xk.z, yk.z, zk.z, 0,
								   0, 0, 0, 1);
	Ruku = glm::transpose(Ruku);
	glm::mat4x4 Tz = glm::mat4x4(1, 0, 0, 0,
								 0, 1, 0, 0,
								 0, 0, -1, 0,
								 0, 0, 0, 1);
	Tz = glm::transpose(Tz);

	matT = T1*Ruku*Tz;

	float H = glm::length(Z);
	glm::mat4x4 P = glm::mat4x4(1, 0, 0, 0,
								0, 1, 0, 0,
								0, 0, 0, (1/H),
								0, 0, 0, 0);
	P = glm::transpose(P);

	matP = P;

	if(false){
		cout<<glm::to_string(viewUp)<<endl;
		cout<<"Ukupna matrica: "<<glm::to_string(matT)<<endl;
			glm::vec4 tocka468 = glm::vec4(4, 6, 8, 1);
			cout<<"Tocka "<<glm::to_string((tocka468*matT))<<endl;
			glm::vec4 AP = (tocka468*matT)*P;
			AP.x = AP.x/AP.w;
			AP.y = AP.y/AP.w;
			cout<<"Tocka projekcija: "<<AP.x<<", "<<AP.y<<endl;
	}
}

void postaviPozicije(){
	Vrh vrh = Vrh{20,0,-20};
	pozicijeOcista.push_back(vrh);
	vrh = Vrh{20,20,-20};
	pozicijeOcista.push_back(vrh);
	vrh = Vrh{20,20,-40};
	pozicijeOcista.push_back(vrh);
	vrh = Vrh{0,0,-20};
	pozicijeOcista.push_back(vrh);
}

void loadModel(string fileName){

	bool firstVertex = true;
	string line;
	Vrh suma = {0,0,0};
	ifstream myfile (fileName);
	if (myfile.is_open()){
		while (getline (myfile,line)){
			if(line[0] == 'v'){
				vector<string> vertices = split(line, " ");
				
				Vrh vrh = {stof(vertices[1])*100,stof(vertices[2])*100,stof(vertices[3])*100};
				vrhovi.push_back(vrh);
				prikazaniVrhovi.push_back(vrh);
				
				if(firstVertex){
					firstVertex = false;
					minx = vrh.x;
					maxx = vrh.x;
					miny = vrh.y;
					maxy = vrh.y;
					minz = vrh.z;
					maxz = vrh.z;
				}
				else{
					if(minx > vrh.x){
						minx = vrh.x;
					}
					if(maxx < vrh.x){
						maxx = vrh.x;
					}
					if(miny > vrh.y){
						miny = vrh.y;
					}
					if(maxy < vrh.y){
						maxy = vrh.y;
					}
					if(minz > vrh.z){
						minz = vrh.z;
					}
					if(maxz < vrh.z){
						maxz = vrh.z;
					}
				}
			}
			else if(line[0] == 'f'){
				vector<string> polygons = split(line, " ");
				
				Poligon poligon = {stoi(polygons[1]),stoi(polygons[2]),stoi(polygons[3])};
				poligoni.push_back(poligon);
			}
		}
		myfile.close();
		
		
	}
	//ispis ucitanih podataka
	if(false){
		for(int i = 0; i < vrhovi.size(); i++){
		cout << "Vrh "<<i<<":  "<<vrhovi[i].x << " "<< vrhovi[i].y << " " << vrhovi[i].z <<endl;
		}

		for(int i = 0; i < poligoni.size(); i++){
			cout << "Poligon "<<i<<":  " <<poligoni[i].vrh1<<" "<<poligoni[i].vrh2<<" "<< poligoni[i].vrh3 << endl;
		}
	}
	

	srednjix = (minx + maxx)/2;
	srednjiy = (miny + maxy)/2;
	srednjiz = (minz + maxz)/2;
	maxRaspon = maxx - minx;
	float yRaspon = maxy - miny;
	if(maxRaspon < yRaspon){
		maxRaspon = yRaspon;
	}
	float zRaspon = maxz - minz;
	if(maxRaspon < zRaspon){
		maxRaspon = zRaspon;
	}

	for(int i = 0; i < vrhovi.size(); i++){
		vrhovi[i].x = (vrhovi[i].x-srednjix)*(2/maxRaspon);
		vrhovi[i].y = (vrhovi[i].y-srednjiy)*(2/maxRaspon);
		vrhovi[i].z = (vrhovi[i].z-srednjiz)*(2/maxRaspon);
		//cout << "Normirani vrh "<<i<<":  "<<vrhovi[i].x << " "<< vrhovi[i].y << " " << vrhovi[i].z <<endl;

	}

	
}

void printVector(vector<string> vec){
	for(int i=0; i<vec.size(); i++){
		cout<<vec[i]<<", ";
	}
	cout<<"\n"<<endl;
}

void loadBSpline(string fileName){
	string line;
	ifstream myfile (fileName);

	minx = miny = minz = std::numeric_limits<float>::max();
	maxx = maxy = maxz = std::numeric_limits<float>::min();

	Vrh suma = {0,0,0};

	if (myfile.is_open()){
		while (getline (myfile,line)){
			vector<string> vertices = split(line, " ");
			glm::vec3 vrh = glm::vec3(stof(vertices[0]),stof(vertices[1]),stof(vertices[2]));

			kontrolniPoligon.push_back(vrh);
			if(vrh.x > maxx) maxx=vrh.x;
			if(vrh.x < minx) minx=vrh.x;
			if(vrh.y > maxy) maxy=vrh.y;
			if(vrh.y < miny) miny=vrh.y;
			if(vrh.z > maxz) maxz=vrh.z;
			if(vrh.z < minz) minz=vrh.z;
		}
	}
	myfile.close();



	if(false){
		for(int i=0; i<kontrolniPoligon.size(); i++){
			cout<<glm::to_string(kontrolniPoligon[i])<<endl;
	}
	}

	srednjix = (minx + maxx)/2;
	srednjiy = (miny + maxy)/2;
	srednjiz = (minz + maxz)/2;
	maxRaspon = maxx - minx;
	float yRaspon = maxy - miny;
	if(maxRasponBspline < yRaspon){
		maxRasponBspline = yRaspon;
	}
	float zRaspon = maxz - minz;
	if(maxRasponBspline < zRaspon){
		maxRasponBspline = zRaspon;
	}


	
}

void loadData(){
	
	string line;
	ifstream myfile ("objekti/data1.txt");
	if (myfile.is_open()){
		vector<string> imeModela;
		vector<string> imeBsplajn;
		while (getline (myfile,line)){

			if(line[0] == 'G'){
				vector<string> koordinate = split(line, " ");
		
				glediste.x = stof(koordinate[1]);
				glediste.y = stof(koordinate[2]);
				glediste.z = stof(koordinate[3]);
			}
			else if(line[0] == 'O'){
				vector<string> koordinate = split(line, " ");
				ociste.x = stof(koordinate[1]);
				ociste.y = stof(koordinate[2]);
				ociste.z = stof(koordinate[3]);
			}
			else if(line[0] == 'M'){
				imeModela = split(line, " ");
			}
			else if(line[0] == 'U'){
				vector<string> koordinate = split(line, " ");
				
				viewUp = glm::vec3(stof(koordinate[1]), stof(koordinate[2]), stof(koordinate[3]));
				
			}
			else if(line[0] == 'B'){
				imeBsplajn = split(line, " ");
			}
			else if(line[0] == 'o'){
				vector<string> koordinate = split(line, " ");
				pocetnaOrijentacija = {stof(koordinate[1]),stof(koordinate[2]),stof(koordinate[3])};
			}
		}
		myfile.close();
		loadModel("objekti/"+imeModela[1]);
		loadBSpline("objekti/"+imeBsplajn[1]);
	}
		
}

//=======================B-SPLINE=================================
int factorial(int n){
	int fact = 1; 
	for(int i = 2; i <= n; i++){
		fact*=i;
	}
	return fact;
}

void calculateBspline(){
	//broj segmenata
	int n = kontrolniPoligon.size() - 3;
	glm::mat4x4 B = {-1,3,-3,1,3,-6,0,4,-3,3,3,1,1,0,0,0};
	glm::mat4x3 B2 = {-1,2,-1,3,-4,0,-3,2,1,1,0,0};
	B *= 1.0f/6.0f;
	B2 *= 1.0f/2;

	Vrh suma = {0,0,0};

	for(int i = 1; i <= n; i++){
		glm::mat3x4 Ri = glm::transpose(glm::mat4x3(kontrolniPoligon[i-1],kontrolniPoligon[i],kontrolniPoligon[i+1],kontrolniPoligon[i+2]));
		for(float t = 0; t <= 1.0; t+=0.01f){
			glm::vec4 T = glm::vec4(pow(t,3),pow(t,2),t,1);
			glm::vec3 vrh = T*B*Ri;
			bsplajnVrhovi.push_back(vrh);

			suma.x += vrh.x;
			suma.y += vrh.y;
			suma.z += vrh.z;
			
			
			glm::vec3 T2 = glm::vec3(pow(t,2),t,1);
			glm::vec3 ciljnaTangenta = T2*B2*Ri;

			glm::vec3 pocOr;
			if(bsplajnTangente.size()==0){
				pocOr = pocetnaOrijentacija;
			}
			else{
				pocOr = bsplajnTangente[bsplajnTangente.size()-1];
			}

			ciljnaTangenta = ciljnaTangenta / glm::length(ciljnaTangenta);
			bsplajnTangente.push_back(ciljnaTangenta);

			glm::vec3 osRotacije = glm::cross(pocetnaOrijentacija,ciljnaTangenta);
			double cosfi = glm::dot(pocetnaOrijentacija,ciljnaTangenta)/(glm::length(pocetnaOrijentacija)*glm::length(ciljnaTangenta));
			double fi = glm::acos(cosfi) * (180/M_PI);

			osiRotacije.push_back(osRotacije);
			kuteviRotacije.push_back(fi);

			//druge derivacije
			
			glm::vec3 T3 = glm::vec3(2*t, 1, 0);
			glm::vec3 drugaDer = T3*B2*Ri;
			glm::vec3 normala = glm::cross(ciljnaTangenta, drugaDer);
			normala = normala / glm::length(normala);
			bsplajnNormale.push_back(normala);
			glm::vec3 binormala = glm::cross(ciljnaTangenta, normala);
			binormala = binormala / glm::length(binormala);
			bsplajnBinormale.push_back(binormala);
			

			if(false && t==0 && i==1){
				cout<<glm::to_string(T)<<endl;
				cout<<glm::to_string(B)<<endl;
				cout<<glm::to_string(Ri)<<endl;
				cout<<glm::to_string(T*B)<<endl;
				cout<<glm::to_string(T*B*Ri)<<endl;
			}
			
		}
	}


	int nVrh = bsplajnVrhovi.size();
	suma.x /= nVrh;
	suma.y /= nVrh;
	suma.z /= nVrh;
	glediste.x = suma.x;
	glediste.y = suma.y;
	glediste.y = suma.y;
	
}

glm::vec3 scaleVector(glm::vec3 vec, float maxRaspon){
	float scale = screenCenterX;
	if(screenCenterY < screenCenterX){
		scale = screenCenterY;
	}
	scale = scale/maxRaspon*2;
	glm::vec3 vrh = {vec.x, vec.y, vec.z};
	vrh.x = vrh.x * scale;
	vrh.y = vrh.y * scale;
	vrh.z = vrh.z * scale;
	return vrh;
}

void drawLines(){
	//cout<<"pozvan drawLines"<<endl;
	


	glBegin(GL_LINE_STRIP);
	for(int i= 0; i<bsplajnVrhovi.size(); i++){
		glm::vec3 vrh = bsplajnVrhovi[i];
		Vrh v = {vrh.x, vrh.y, vrh.z};
		v = translateToCenter(projectVertex(v));
		glVertex3f(v.x, v.y, v.z);

	}

	glEnd();


} 

void drawTangent(){
	
	glm::vec3 tangenta = bsplajnTangente[iteracijaAnimacije];
	glm::vec3 pocToc = bsplajnVrhovi[iteracijaAnimacije];
	glm::vec3 zavrsnaToc = pocToc + tangenta*10.0f;
	if(false && iteracijaAnimacije<10){
		cout<<"pocetna"<<endl;
		cout<<glm::to_string(pocToc)<<endl;
		cout<<"zavrsna"<<endl;
		cout<<glm::to_string(zavrsnaToc)<<endl;
		cout<<"duljina"<<endl;
		cout<<glm::length(tangenta)<<endl;
	}
	glColor3f(0.5f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	Vrh v = {pocToc.x, pocToc.y, pocToc.z};
	v = translateToCenter(projectVertex(v));
	glVertex2f(v.x, v.y);
	Vrh v2 = {zavrsnaToc.x, zavrsnaToc.y, zavrsnaToc.z};
	v2 = translateToCenter(projectVertex(v2));
	glVertex2f(v2.x, v2.y);
	glEnd();
}



//===========================================================

Vrh projectVertex(Vrh vrh){
	glm::vec4 v = glm::vec4(vrh.x, vrh.y, vrh.z, 1);
	v = v*matT*matP;
	Vrh pov = {v.x/v.w, v.y/v.w, v.z};
	return pov;
}

Vrh translateToCenter(Vrh vrh){
	float scale = screenCenterX;
	if(screenCenterY < screenCenterX){
		scale = screenCenterY;
	}

	scale /= 4;
	
	vrh.x = vrh.x * scale + screenCenterX;
	vrh.y = vrh.y * scale + screenCenterY;
	return vrh;
}

void printVrh(Vrh vrh){
	cout<<vrh.x<<", "<<vrh.y<<", "<<vrh.z<<endl;
}


float testPoint(glm::vec4 point, Vrh v1,Vrh v2,Vrh v3){
	

	glm::vec3 vektor1 = glm::vec3( v2.x - v1.x, v2.y - v1.y, v2.z - v1.z );
	glm::vec3 vektor2 = glm::vec3( v3.x - v1.x, v3.y - v1.y, v3.z - v1.z );

	glm::vec3 normala = glm::cross(vektor1, vektor2);

	//cout << "A, B, C: "<<glm::to_string(normala)<<endl;

	float A = normala.x;
	float B = normala.y;
	float C = normala.z;

	//D = -Ax - By - Cz
	float D = -A* v1.x -B* v1.y -C* v1.z;

	glm::vec4 R = glm::vec4(A, B, C, D);

	float pointPosition = glm::dot(point, R);
	return pointPosition;
		
}

void drawObject(){
	//cout<<"pozvan draw"<<endl;
	calculateViewTransform();
	screenCenterY = height / 2;
	screenCenterX = width / 2;
	glPointSize(1.0);					
	glColor3f(0.0f, 0.0f, 0.0f);	
	
	if(pomaknutiVrhovi.size()==0){
		pomaknutiVrhovi = vrhovi;
	}

	for(int i = 0; i < poligoni.size(); i++){
		Vrh v1 = translateToCenter(projectVertex(pomaknutiVrhovi[poligoni[i].vrh1 - 1]));
		Vrh v2 = translateToCenter(projectVertex(pomaknutiVrhovi[poligoni[i].vrh2 - 1]));
		Vrh v3 = translateToCenter(projectVertex(pomaknutiVrhovi[poligoni[i].vrh3 - 1])); 

		if(false and i==0){
			cout<<"Prije ";
			printVrh(projectVertex(vrhovi[poligoni[i].vrh1 - 1]));
			cout<<"Samo centriran ";
			printVrh(translateToCenter(vrhovi[poligoni[i].vrh1 - 1]));
			cout<<"Sve ";
			printVrh(v1);
		}

		if(removePoligons && testPoint(glm::vec4(ociste.x, ociste.y, ociste.z, 1), v1,v2,v3)>=0){
			continue;
		}
	

		glBegin(GL_LINE_LOOP);
			glVertex2f(v1.x , v1.y);
			glVertex2f(v2.x , v2.y);
			glVertex2f(v3.x , v3.y);
		glEnd();
	}

}

void myDisplay()
{

	glFlush();
}


void myReshape(int w, int h)
{

	width = w; height = h;               							
	glViewport(0, 0, width, height);	
	
	glMatrixMode(GL_PROJECTION);		
	glLoadIdentity();					
	glOrtho(0, width, 0, height, 0, 1); 	
	glMatrixMode(GL_MODELVIEW);			
	glLoadIdentity();					

	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);		
	glPointSize(1.0);					
	glColor3f(0.0f, 0.0f, 0.0f);	
	drawLines();
	drawTangent();
	drawObject();

}

	

void rotateGlediste(char os, int smjer){
	float sin15 = smjer*0.08716f;
	float cos15 = 0.99619f; 
	glm::mat4x4 matRot;
	if(os=='y'){
		matRot= glm::mat4x4(cos15, 0, sin15, 0,
							0, 1, 0, 0,
							-sin15, 0, cos15, 0,
							0, 0, 0, 1);
	}
	else if(os=='x'){
		matRot= glm::mat4x4(1, 0, 0, 0,
							0, cos15, -sin15, 0,
							0, sin15, cos15, 0,
							0, 0, 0, 1);
	}
	matRot = glm::transpose(matRot);
	glm::mat4x4 T1 = glm::mat4x4(1, 0, 0, 0,
								 0, 1, 0, 0,
								 0, 0, 1, 0,
								 -ociste.x, -ociste.y, -ociste.z, 1);
	T1 = glm::transpose(T1);

	glm::mat4x4 T2 = glm::mat4x4(1, 0, 0, 0,
								 0, 1, 0, 0,
								 0, 0, 1, 0,
								 ociste.x, ociste.y, ociste.z, 1);
	T2 = glm::transpose(T2);
	glm::mat4x4 matUku = T1*matRot*T2;
	glm::vec4 G = glm::vec4(glediste.x, glediste.y, glediste.z, 1);
	G = G*matUku;
	glediste.x = G.x;
	glediste.y = G.y;
	glediste.z = 0;

	
}

int wasteTime(){
	int antiOptimizationSum = 0;
	int n = 20000;
	if(vrhovi.size()>100){
		n=0;
	}
	for(int i=0; i < n; i++){
		antiOptimizationSum+=1;
	}
	return antiOptimizationSum;
}

Vrh vectorToVrh(glm::vec3 v){
	Vrh vrh = {v.x, v.y, v.z};
	return vrh;
}

glm::vec3 vrhToVector(Vrh v){
	glm::vec3 vec = {v.x, v.y, v.z};
	return vec;
}

void startAnimation(){
	int n = bsplajnVrhovi.size();
	
	for(int i = 0; i < n; i++){
		iteracijaAnimacije = i;

		Vrh pomak = vectorToVrh(bsplajnVrhovi[i]);
		pomaknutiVrhovi.clear();
		glm::mat3x3 rotacijskaMat = {bsplajnTangente[iteracijaAnimacije], bsplajnNormale[iteracijaAnimacije], bsplajnBinormale[iteracijaAnimacije]};
		rotacijskaMat = glm::transpose(rotacijskaMat);

		glm::vec3 w = osiRotacije[iteracijaAnimacije];
		double fi = kuteviRotacije[iteracijaAnimacije];
		float cosfi = glm::cos(fi);
		float sinfi = glm::sin(fi);

		for(int j = 0; j < vrhovi.size(); j++){
			glm::vec3 noviVrh1 = {vrhovi[j].x,vrhovi[j].y,vrhovi[j].z};
			Vrh noviVrh;
			if(useDCM){
				noviVrh = vectorToVrh(noviVrh1*rotacijskaMat);
			}
			else{
				noviVrh = vectorToVrh(noviVrh1*cosfi + glm::cross(w, noviVrh1)*sinfi + w*glm::dot(w,noviVrh1)*(1-cosfi));

			}
			
			noviVrh.x += pomak.x;
			noviVrh.y += pomak.y;
			noviVrh.z += pomak.z;
			pomaknutiVrhovi.push_back(noviVrh);
		}
		
		
		myReshape(width, height);
		glFlush();
		wasteTime();
	}
}

void myMouse(int button, int state, int x, int y)
{
	
	
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	int speed = 10;
	float lookSpeed = 0.5f;
	//printVrh(glediste);
	switch (theKey)
	{
	case 'w':
		
		ociste.z += speed;
		break;

	case 'd':
		ociste.x += speed;
		break;

	case 's':
		ociste.z -= speed;
		break;

	case 'a':
		ociste.x -= speed;
		break;
	case 'e':
		ociste.y -= speed;
		break;

	case 'q':
		ociste.y += speed;
		break;
	case 'r':
		ociste = pozicijeOcista[indexOcista];
		indexOcista++;
		if(indexOcista>=pozicijeOcista.size()){
			indexOcista=0;
		}
		glediste.x = 0;
		glediste.y = 0;
		glediste.z = 0;
		break;
	case 'i':
		glediste.y += lookSpeed;
		break;
	
	case 'k':
		glediste.y -= lookSpeed;
		break;
	
	case 'j':
		glediste.x += lookSpeed;
		break;
	
	case 'l':
		glediste.x -= lookSpeed;
		break;
	
	case 'b':
		startAnimation();
		break;
	case 'f':
		removePoligons = !removePoligons;
		break;
	case 'n':
		useDCM = !useDCM;
		break;
	}
	myReshape(width, height);

	glFlush();
}







